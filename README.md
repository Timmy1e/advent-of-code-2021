# Advent of Code 2021

My solutions to this year's calendar from [adventofcode.com](https://adventofcode.com/2021).
I'm going to try writing all of them in Rust, just for the fun of it.

I'll be using the [`cargo-aoc`](https://github.com/gobanos/cargo-aoc) for it's benchmarking and fetching of input files.
Running on my M1 Max 16" 2021 MacBook Pro.

## Day 1: Sonar Sweep
Used `Ittr::windows()` for the first time.
### Part 1
Found `1475` in `333.24 ns`.
### Part 2
Found `1516` in `471.99 ns`.

## Day 2: Dive!
This years first instruction following.
### Part 1
Found `1654760` in `857.32 ns`.
### Part 2
Found `1956047400` in `856.84 ns`.

## Day 3: Binary Diagnostic
Bit twiddling and recursion.
### Part 1
Found `4001724` in `3.9409 µs`.
### Part 2
Found `587895` in `12.565 µs`.

## Day 4: Giant Squid
The multi-threading Matrix.
### Part 1
Found `64084` in `930.58 µs`.
### Part 2
Found `12833` in `942.35 µs`.
