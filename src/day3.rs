use aoc_runner_derive::{aoc, aoc_generator};

///# Day 3: Binary Diagnostic
/// https://adventofcode.com/2021/day/3
#[aoc_generator(day3)]
pub fn prepare_input(input: &str) -> Vec<usize> {
    input.lines().map(|s| usize::from_str_radix(s.trim(), 2).unwrap()).collect()
}

const BITS: usize = 12;
const INV: usize = 0b111111111111;

///## What is the power consumption of the submarine?
#[aoc(day3, part1)]
pub fn solve_part1(input: &Vec<usize>) -> usize {
    let size = input.len();
    let bit_sizes = &mut vec![0usize; BITS];

    for line in input {
        let mut current_bit = 1usize << BITS;
        for bit in 0..BITS {
            current_bit >>= 1;
            if line & current_bit > 0 {
                bit_sizes[bit] += 1;
            }
        }
    }

    let mut gamma = 0;
    for (index, &bit_size) in bit_sizes.iter().enumerate() {
        if bit_size > size / 2 {
            let mut bit_to_set = 1usize;
            bit_to_set <<= (BITS - 1) - index;
            gamma |= bit_to_set;
        }
    }

    gamma * (gamma ^ INV)
}

///## What is the life support rating of the submarine?
#[aoc(day3, part2)]
pub fn solve_part2(input: &Vec<usize>) -> usize {
    // Recursively solve the O2 and CO2 problems.
    fn recursion(use_most_common_bit: bool, lines: &Vec<usize>, bit: usize) -> usize {
        let mut ones = vec![];
        let mut zeros = vec![];
        let current_bit = 1usize << (BITS - bit);

        for &line in lines {
            if line & current_bit > 0 {
                ones.push(line);
            } else {
                zeros.push(line);
            }
        }

        let (remainder, is_ones) = if (use_most_common_bit && ones.len() >= zeros.len()) || (!use_most_common_bit && zeros.len() > ones.len()) {
            (&ones, true)
        } else {
            (&zeros, false)
        };

        if remainder.len() < 2 {
            if is_ones {
                *ones.first().unwrap()
            } else {
                *zeros.first().unwrap()
            }
        } else if bit == BITS {
            panic!("Hit the end of the bits!")
        } else {
            recursion(use_most_common_bit, remainder, bit + 1)
        }
    }

    let o2 = recursion(true, input, 1);
    let co2 = recursion(false, input, 1);

    o2 * co2
}

