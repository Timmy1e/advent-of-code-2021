use aoc_runner_derive::{aoc, aoc_generator};

///# Day 2: Dive!
/// https://adventofcode.com/2021/day/2
#[aoc_generator(day2)]
pub fn prepare_input(input: &str) -> Vec<SubmarineInstruction> {
    input.lines().map(|s| {
        let mut split = s.trim().split(' ');
        // Parse the lines as instructions
        match split.next().unwrap() {
            "forward" => SubmarineInstruction::Forward(split.next().unwrap().parse().unwrap()),
            "down" => SubmarineInstruction::Down(split.next().unwrap().parse().unwrap()),
            "up" => SubmarineInstruction::Up(split.next().unwrap().parse().unwrap()),
            // Panic if unknown
            _ => panic!("Unknown line!")
        }
    }).collect()
}

///## Calculate the horizontal position and depth you would have after following the planned course.
///## What do you get if you multiply your final horizontal position by your final depth?
#[aoc(day2, part1)]
pub fn solve_part1(input: &Vec<SubmarineInstruction>) -> usize {
    let mut position = (0, 0);

    for instruction in input {
        match instruction {
            SubmarineInstruction::Forward(x) => position.0 += x,
            SubmarineInstruction::Down(y) => position.1 += y,
            SubmarineInstruction::Up(y) => position.1 -= y
        }
    }

    position.0 * position.1
}

///## Using this new interpretation of the commands, calculate the horizontal position and depth you would have after following the planned course.
///## What do you get if you multiply your final horizontal position by your final depth?
#[aoc(day2, part2)]
pub fn solve_part2(input: &Vec<SubmarineInstruction>) -> usize {
    let mut position = (0, 0);
    let mut aim = 0;

    for instruction in input {
        match instruction {
            SubmarineInstruction::Forward(x) => {
                position.0 += x;
                position.1 += x * aim;
            },
            SubmarineInstruction::Down(y) => aim += y,
            SubmarineInstruction::Up(y) => aim -= y
        }
    }

    position.0 * position.1
}

pub enum SubmarineInstruction {
    Forward(usize),
    Down(usize),
    Up(usize),
}
