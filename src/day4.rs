use std::thread::{spawn, JoinHandle};

use aoc_runner_derive::{aoc, aoc_generator};

/// # Day 4: Giant Squid
/// https://adventofcode.com/2021/day/4
#[aoc_generator(day4)]
pub fn prepare_input(input: &str) -> Day4Obj {
    let mut lines = input.lines();
    let numbers = lines.nth(0).unwrap().trim().split(',').map(|s| s.trim().parse().unwrap()).collect();
    let mut boards = vec![];
    let mut rows = Vec::with_capacity(5);
    lines.next();

    for window in lines {
        if window.is_empty() {
            boards.push(BingoBoard::new(rows));
            rows = Vec::with_capacity(5);
            continue;
        }
        rows.push(window.trim().split(' ').filter(|&x| !x.is_empty()).map(|s| BingoBoardSpace::new(s.trim().parse::<usize>().unwrap())).collect::<Vec<BingoBoardSpace>>()
        );
    }
    boards.push(BingoBoard::new(rows));

    Day4Obj {
        numbers,
        boards,
    }
}

///## To guarantee victory against the giant squid, figure out which board will win first. What will your final score be if you choose that board?
#[aoc(day4, part1)]
pub fn solve_part1(input: &Day4Obj) -> usize {
    let threads = solve_boards(input);

    let mut answer = (usize::MAX, 0);
    for thread in threads {
        if let Ok((turns, result)) = thread.join() {
            if result != 0 && turns < answer.0 {
                answer = (turns, result);
            }
        }
    }

    answer.1
}

///## Figure out which board will win last. Once it wins, what would its final score be?
#[aoc(day4, part2)]
pub fn solve_part2(input: &Day4Obj) -> usize {
    let threads = solve_boards(input);

    let mut answer = (usize::MIN, 0);
    for thread in threads {
        if let Ok((turns, result)) = thread.join() {
            if result != 0 && turns > answer.0 {
                answer = (turns, result);
            }
        }
    }

    answer.1
}

fn solve_boards(input: &Day4Obj) -> Vec<JoinHandle<(usize, usize)>> {
    let mut threads = vec![];

    for board in &input.boards {
        let mut board = board.clone();
        let numbers = input.numbers.clone();
        threads.push(spawn(move || {
            for number in numbers {
                board.turns += 1;

                if !board.board.iter_mut().flatten().any(|bbs| if bbs.value == number {
                    bbs.mark();
                    board.marked += 1;
                    true
                } else { false }) {
                    continue;
                }

                if board.marked > 4 && check_board_for_bingo(&board.board) {
                    let sum: usize = board.board.iter().flatten().filter_map(|bbs| if !bbs.is_marked {
                        Some(bbs.value)
                    } else {
                        None
                    }).sum();

                    return (board.turns, sum * number);
                }
            }

            (board.turns, 0)
        }));
    }

    threads
}

fn check_board_for_bingo(board: &Vec<Vec<BingoBoardSpace>>) -> bool {
    for (index, row) in board.iter().enumerate() {
        // Check the row
        if row.iter().all(|x| x.is_marked) {
            return true;
        }

        // Check the column index
        if board.iter().all(|row| row.get(index).unwrap().is_marked) {
            return true
        }
    }

    false
}

pub struct Day4Obj {
    numbers: Vec<usize>,
    boards: Vec<BingoBoard>,
}

#[derive(Clone)]
struct BingoBoard {
    turns: usize,
    marked: usize,
    board: Vec<Vec<BingoBoardSpace>>,
}

impl BingoBoard {
    const fn new(rows: Vec<Vec<BingoBoardSpace>>) -> BingoBoard {
        BingoBoard {
            turns: 0,
            marked: 0,
            board: rows,
        }
    }
}

#[derive(Clone)]
struct BingoBoardSpace {
    value: usize,
    is_marked: bool,
}

impl BingoBoardSpace {
    const fn new(value: usize) -> BingoBoardSpace {
        BingoBoardSpace {
            value,
            is_marked: false,
        }
    }

    fn mark(&mut self) {
        self.is_marked = true;
    }
}
