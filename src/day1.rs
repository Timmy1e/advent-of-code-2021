use aoc_runner_derive::{aoc, aoc_generator};

///# Day 1: Sonar Sweep
/// https://adventofcode.com/2021/day/1
#[aoc_generator(day1)]
pub fn prepare_input(input: &str) -> Vec<usize> {
    input.lines().map(|s| s.trim().parse().unwrap()).collect()
}

///## How many measurements are larger than the previous measurement?
#[aoc(day1, part1)]
pub fn solve_part1(input: &Vec<usize>) -> usize {
    let mut result = 0;
    let mut previous = &usize::MAX;

    for val in input {
        if val > &previous {
            result += 1;
        }
        previous = val;
    }

    result
}

///## How many sums are larger than the previous sum?
#[aoc(day1, part2)]
pub fn solve_part2(input: &Vec<usize>) -> usize {
    let mut result = 0;
    let mut previous = usize::MAX;

    for vals in input.windows(3) {
        let sum = vals.iter().sum::<usize>();
        if sum > previous {
            result += 1;
        }
        previous = sum;
    }

    result
}
